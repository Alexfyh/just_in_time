/*
===============================================================================
 Name        : Just_in_Time.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif
#include <cr_section_macros.h>
#include <stdlib.h>
#include <string.h>

void EINT3_IRQHandler(void);
void RTC_IRQHandler(void);
void RTC_initialConfig(void);
void RTC_setClock(int,int,int);
void RTC_clearClock();
void RTC_setAlarm(int,int,int);
void RTC_clearAlarm();
void RTC_setAuxiliar();
void RTC_clearAuxiliar();
int testSettingValue(int, int,int);

void UART0_IRQHandler(void);
void UART_initialConfig(void);
void convert_ASCII_to_int(void);
void TX_enviar(char []);
static int Contador_Entradas = 0;
void config_Teclado_Matricial(void);

int buscarFila(int);
int antideboucing(int);
void retardo(int);

void actualizarCadena(int, int, int,char *, int);
static char salida_TX [60];
static char Cadena_horaActual[9];
static char Cadena_horaAlarma[6];
static char Cadena_horaAuxiliar[6];

void BuzzerConfig(void);
void Buzzer_sonar(void);

void ADC_config(void);
void ADC_inicioConversion(void);
void ADC_IRQHandler(void);

void TIMER_config(void);
void TIMER0_IRQHandler(void);

#define CANT_ARGUMENTOS 7
static char inputs[CANT_ARGUMENTOS];
static int arguments[CANT_ARGUMENTOS];

void execute(int[]);
void armarInstruccion(char);
/*
 * Definicion de Comandos
 */
#define SET_CLOCK		65
#define SET_ALARM		66
#define SET_AUXILIAR	67
#define CLEAR_CLOCK		68
#define CLEAR_ALARM		69
#define CLEAR_AUXILIAR	70

#define DEFAULT_HOUR	12
#define DEFAULT_MINUTE	30
#define DEFAULT_SECOND	0
#define DEFAULT_DOM		27
#define DEFAULT_MONTH	10
#define DEFAULT_YEAR	2018
static char  TECLADO_entradas[4][4]= {{'1','2','3',SET_CLOCK},
							{'4','5','6',SET_ALARM},
							{'7','8','9',SET_AUXILIAR},
							{CLEAR_AUXILIAR,'0',CLEAR_ALARM,CLEAR_CLOCK}};
static int ALARM_HOUR_BACKUP	=0;
static int ALARM_MIN_BACKUP		=0;
static int AUXILIAR_HOUR_BACKUP	=0;
static int AUXILIAR_MIN_BACKUP	=0;
#define AUXILIAR_ENABLE 1
#define AUXILIAR_DISABLE 0
static int auxiliarState;
#define MODO_PENDIENTE 0
#define MODO_NORMAL  1
static int MODO;

int main(void) {
	SystemInit();
	int a =SystemCoreClock;
	LPC_GPIO0->FIOCLR |= (1<<0);

	TIMER_config();
	NVIC_EnableIRQ(TIMER0_IRQn);

	ADC_config();
	NVIC_EnableIRQ(ADC_IRQn);

	BuzzerConfig();
	config_Teclado_Matricial();
	NVIC_EnableIRQ(EINT3_IRQn);

	RTC_initialConfig();
	NVIC_EnableIRQ(RTC_IRQn);

	UART_initialConfig();
	NVIC_EnableIRQ(UART0_IRQn);

	int sec,min, hour;
	while(1){
		sec=LPC_RTC->SEC;
		min=LPC_RTC->MIN;
		hour=LPC_RTC->HOUR;
	}
}

void RTC_initialConfig(void){
	LPC_RTC->CIIR |=(3<<0);					//Habilito inter por minuto y seg
	LPC_RTC->CCR = ((1 << 1 ) | (1 << 4));
	LPC_RTC->CALIBRATION = 0x00;
	LPC_RTC->CCR = (1 << 0);
	/*
	 * Fecha por default
	 */
	LPC_RTC->SEC 	= DEFAULT_SECOND;
	LPC_RTC->MIN 	= DEFAULT_MINUTE;
	LPC_RTC->HOUR	= DEFAULT_HOUR;
	LPC_RTC->DOM 	= DEFAULT_DOM;
	LPC_RTC->MONTH	=DEFAULT_MONTH;
	LPC_RTC->YEAR	=DEFAULT_YEAR;
	/*
	 * Limpio banderas por si quedó guardada alguna
	 */
	LPC_RTC->ILR |= (1<<0);
	LPC_RTC->ILR |= (1<<1);

	LPC_RTC->AMR |= 0xFF;							//ALARM DISABLE
	auxiliarState=AUXILIAR_DISABLE;
}
void RTC_IRQHandler(void){
	if(LPC_RTC->ILR&(1<<0)){						//Se produjo interrupcion por incremento?
		if(LPC_RTC->CIIR&(1<<0)){					//Incremento el segundo?
			//TODO enviar a LCD la siguiente cadena_horaActual
			actualizarCadena(LPC_RTC->HOUR,LPC_RTC->MIN,LPC_RTC->SEC,Cadena_horaActual,9);
		}
		if(LPC_RTC->CIIR&(1<<1)){					//Incremento el minuto?
			/*
			 * Pregunta sólo si la alarma auxiliar está configurada, si se encuentra
			 * en el intervalo entre la alarma auxiliar y la alarma principal INCLUIDOS?
			 */
			//TODO ver caso si el switch esta en OFF
			if((auxiliarState==AUXILIAR_ENABLE)
					&&((LPC_RTC->HOUR>AUXILIAR_HOUR_BACKUP)||
							((LPC_RTC->HOUR==AUXILIAR_HOUR_BACKUP)&&(LPC_RTC->MIN>AUXILIAR_MIN_BACKUP)))
					&&((LPC_RTC->HOUR<ALARM_HOUR_BACKUP)||
							((LPC_RTC->HOUR==ALARM_HOUR_BACKUP)&&(LPC_RTC->MIN<=ALARM_MIN_BACKUP)))
					&&(LPC_GPIO0->FIOPIN&(1<<1))){
				MODO=MODO_PENDIENTE;
				LPC_RTC->ALHOUR = ALARM_HOUR_BACKUP;
				LPC_RTC->ALMIN = ALARM_MIN_BACKUP;
			}
			else{
				MODO=MODO_NORMAL;
			}
		}
		LPC_RTC->ILR |= (1<<0);							//Limpia la bandera por incremento
	}
	if(LPC_RTC->ILR&(1<<1)){							//Se produjo interrupción por alarma?
		if(LPC_GPIO0->FIOPIN&(1<<1)){					//Buzzer habilitado
			if(auxiliarState==AUXILIAR_DISABLE){		//soy la unica? O sea la alarma principal
				Buzzer_sonar();							//No hace falta que pregunte por la conversión
			}
			else{
				if(MODO==MODO_NORMAL){					//Entra al preguntar por primera vez con alarma Auxiliar
					ADC_inicioConversion();				//Incia la conversión
				}
				else{					//Entra si ya estaba en modo pendiente,o sea se produjo por alarma Principal
					LPC_GPIO0->FIOPIN |= (1<<0);				//Prendo el buzzer
					LPC_RTC->ALHOUR = AUXILIAR_HOUR_BACKUP;		//Seteo la alarma para la proxima
					LPC_RTC->ALMIN 	= AUXILIAR_MIN_BACKUP;
				}
			}
		}
		else{

		}
		LPC_RTC->ILR |= (1<<1);						//Limpia bandera por alarma
	}
}

void RTC_setClock(int hour, int min, int sec){
	if(testSettingValue(hour,min,sec)){
		LPC_RTC->HOUR 	= hour;
		LPC_RTC->MIN 	= min;
		LPC_RTC->SEC	= sec;
		actualizarCadena(hour,min,sec,Cadena_horaActual,9);
		strcat(salida_TX,"Setting Clock = ");
		strcat(salida_TX,Cadena_horaActual);
		TX_enviar(salida_TX);
	}
	else{
		strcpy(salida_TX,"No valid Clock");
		TX_enviar(salida_TX);
	}
}
void RTC_clearClock(){
	LPC_RTC->SEC = DEFAULT_SECOND;
	LPC_RTC->MIN = DEFAULT_MINUTE;
	LPC_RTC->HOUR= DEFAULT_HOUR;
	actualizarCadena(DEFAULT_HOUR,DEFAULT_MINUTE,DEFAULT_SECOND,Cadena_horaActual,9);
	strcat(salida_TX,"Default clock = ");
	strcat(salida_TX,Cadena_horaActual);
	TX_enviar(salida_TX);
}
void RTC_setAlarm(int hour, int min,int duration){
	if(testSettingValue(hour,min,0)){
		//Enmascaro todas salvo los minutos y horas para la alarma
		LPC_RTC->AMR |= 0xF8;
		LPC_RTC->AMR &= ~(7<<0);
		if(auxiliarState==AUXILIAR_ENABLE){					//estaba configurada la auxiliar?
			/*
			 * Uso el back up de la alarma auxiliar para comparacion porque puede estar en estado pendiente
			 */
			if((hour>AUXILIAR_HOUR_BACKUP)||((hour==AUXILIAR_HOUR_BACKUP)&&(min>AUXILIAR_MIN_BACKUP))){
				ALARM_HOUR_BACKUP=hour;
				ALARM_MIN_BACKUP=min;
				actualizarCadena(hour,min,0,Cadena_horaAlarma,6);
				strcat(salida_TX,"New Alarm = ");
				strcat(salida_TX,Cadena_horaAlarma);
				TX_enviar(salida_TX);
				//TODO concatenar con strcat la cadena Cadena_horaAlarma con Cadena_horaAuxiliar a la 2° linea LCD
			}
			else{
				TX_enviar("Error: Alarm before Auxiliar");
			}
		}
		else{
			LPC_RTC->ALSEC 	= 0;
			LPC_RTC->ALMIN 	= min;
			LPC_RTC->ALHOUR = hour;
			actualizarCadena(hour,min,0,Cadena_horaAlarma,6);
			strcat(salida_TX,"Setting Alarm = ");
			strcat(salida_TX,Cadena_horaAlarma);
			TX_enviar(salida_TX);
			//TODO concatenar con strcat la cadena Cadena_horaAlarma con Cadena_horaAuxiliar a la 2° linea LCD
		}
	}
	else{
		strcpy(salida_TX,"No valid Alarm");
		TX_enviar(salida_TX);
	}
}

void RTC_clearAlarm(){
	LPC_RTC->AMR |= 0xFF;				//ALARM DISABLE
	auxiliarState=AUXILIAR_DISABLE;
	strcpy(salida_TX,"Clearing Alarm");
	TX_enviar(salida_TX);
	//TODO strcpy una cadena vacia a la cadena Cadena_horaAlarma
	//Todo strcpy una cadena vacia a la cadena Cadena_horaAuxiliar
}

void RTC_setAuxiliar(int hour, int min){
	if(LPC_RTC->AMR==0xFF){			//si no esta configurada la principal
		TX_enviar("Error = No existe alarma principal");
		return;
	}
	if(testSettingValue(hour,min,0)){
		if(auxiliarState){									//ya estaba configurada
			if((hour<ALARM_HOUR_BACKUP)||((hour==ALARM_HOUR_BACKUP)&&(min<ALARM_MIN_BACKUP))){
				LPC_RTC->ALHOUR=hour;
				LPC_RTC->ALMIN=min;
				AUXILIAR_HOUR_BACKUP=hour;
				AUXILIAR_MIN_BACKUP=min;
				actualizarCadena(hour,min,0,Cadena_horaAuxiliar,6);
				strcat(salida_TX,"New Auxiliary Alarm = ");
				strcat(salida_TX,Cadena_horaAuxiliar);
				TX_enviar(salida_TX);
				//TODO concatenar con strcat la cadena Cadena_horaAlarma con Cadena_horaAuxiliar a la 2° linea LCD
			}
			else{
				TX_enviar("Error = Auxiliary alarm after Alarm");
			}
		}
		else{
			/*
			 * Hago un back up de la alarma principal
			 */
			if((hour<LPC_RTC->ALHOUR)||((hour==LPC_RTC->ALHOUR)&&(min<LPC_RTC->ALMIN))){
				auxiliarState=AUXILIAR_ENABLE;
				ALARM_HOUR_BACKUP=LPC_RTC->ALHOUR;
				ALARM_MIN_BACKUP=LPC_RTC->ALMIN;
				LPC_RTC->ALHOUR=hour;
				LPC_RTC->ALMIN=min;
				AUXILIAR_HOUR_BACKUP=hour;
				AUXILIAR_MIN_BACKUP=min;
				actualizarCadena(hour,min,0,Cadena_horaAuxiliar,6);
				strcat(salida_TX,"New Auxiliary Alarm = ");
				strcat(salida_TX,Cadena_horaAuxiliar);
				TX_enviar(salida_TX);
				//TODO concatenar con strcat la cadena Cadena_horaAlarma con Cadena_horaAuxiliar a la 2° linea LCD
			}
			else{
				TX_enviar("Error = Auxiliary alarm after Alarm");
			}
		}
	}
	else{
		strcpy(salida_TX,"No valid Auxiliar Alarm");
		TX_enviar(salida_TX);
	}
}

void RTC_clearAuxiliar(){
	if(auxiliarState==AUXILIAR_DISABLE){
		TX_enviar("Error = No había alarma auxiliar por borrar");
		return;
	}
	auxiliarState=AUXILIAR_DISABLE;
	LPC_RTC->ALHOUR=ALARM_HOUR_BACKUP;
	LPC_RTC->ALMIN=ALARM_MIN_BACKUP;
	strcpy(salida_TX,"Clearing Auxiliar Alarm");
	TX_enviar(salida_TX);
}
int testSettingValue(int hour, int min,int sec){
	if((hour<0)||(hour>23)||(min<0)||(min>59)||(sec<0)||(sec>59)){
		return 0;
	}
	else{
		return 1;
	}
}

void UART_initialConfig(){
	//On reset UART0 is enable
	//On reset PCLKSEL = 00  =>CCLK/4=20MHz/4=5MHz
	LPC_UART0->LCR |= (3<<0);							//8 bits de data
	LPC_UART0->LCR &= ~(1<<2);							// 1 bit de parada
	LPC_UART0->LCR &= ~(1<<3);							//Sin checksum de paridad
	LPC_UART0->LCR |= (1<<7);							//Habilito acceso al divisor de latch

	LPC_UART0->DLL = 34;								//34,133
	LPC_UART0->DLM = 0;

	LPC_UART0->LCR &= ~(1<<7);							//Deshabilito acceso al divisor de latch
	LPC_UART0->FCR |= (1<<0);							//Habilito FIFOs
	LPC_UART0->FCR |= (3<<1);							//Reseto FIFOs para Tx y Rx

	LPC_PINCON->PINSEL0 &= ~(15<<4);					//Pongo en 0 P0.2 y P0.3
	LPC_PINCON->PINSEL0 |= (1<<4);						//P0.2 como TXD0
	LPC_PINCON->PINSEL0 |= (1<<6);						//P0.3 como RXD0
	//Es obligatorio que el pinmode del receptor no sea pull-down
	LPC_PINCON->PINMODE0 &=~(3<<6);
	LPC_PINCON->PINMODE0 |= (1<<7);

	//LPC_UART0->IER |= (3<<0);							//Habilito inter por Tx  y Rx
	LPC_UART0->IER |= (1<<0);
	LPC_UART0->IER &= ~(1<<1);
}

void UART0_IRQHandler(void){
	armarInstruccion((char)LPC_UART0->RBR);
}

void armarInstruccion(char  entrada){
	inputs[Contador_Entradas%CANT_ARGUMENTOS]= entrada;
	if((Contador_Entradas%CANT_ARGUMENTOS)==0){					//Pregunta por si hace falta rellenar
		switch (inputs[0]) {
			case SET_CLOCK:
				break;
			case SET_ALARM:
				Contador_Entradas = Contador_Entradas+2;
				break;
			case SET_AUXILIAR:
				Contador_Entradas= Contador_Entradas+2;
				break;
			case CLEAR_CLOCK:
				Contador_Entradas=Contador_Entradas+6;
				break;
			case CLEAR_ALARM:
				Contador_Entradas=Contador_Entradas+6;
				break;
			case CLEAR_AUXILIAR:
				Contador_Entradas=Contador_Entradas+6;
				break;
			default:
				break;
		}
	}
	Contador_Entradas++;
	if((Contador_Entradas%CANT_ARGUMENTOS)==0){				//Se completó una conjunto de argumentos?
		convert_ASCII_to_int();
		execute(arguments);
	}
}

void convert_ASCII_to_int(void){
	//No convierte el primero porque es el comando
	arguments[0] = (int) inputs[0];
	for (int var = 1; var < CANT_ARGUMENTOS; ++var) {
		arguments[var] = (int) inputs[var]-48;
	}
}

void execute(int args [] ){
	switch (args[0]) {
		case SET_CLOCK:
			RTC_setClock(args[1]*10+args[2],args[3]*10+args[4],args[5]*10+args[6]);
			break;
		case SET_ALARM:
			RTC_setAlarm(args[3]*10+args[4],args[5]*10+args[6],0);
			break;
		case SET_AUXILIAR:
			RTC_setAuxiliar(args[3]*10+args[4],args[5]*10+args[6]);
			break;
		case CLEAR_CLOCK:
			RTC_clearClock();
			break;
		case CLEAR_ALARM:
			RTC_clearAlarm();
			break;
		case CLEAR_AUXILIAR:
			RTC_clearAuxiliar();
			break;
		default:
			break;
	}
}

void EINT3_IRQHandler(void){
	if(LPC_GPIOINT->IntStatus&&(1<<0)){					//se produjo interrupcion por puerto 0
		if(LPC_GPIOINT->IO0IntStatF&(1<<1)){			//Se produjo por flanco de caida P0.1
			//TODO es necesario el antirebote?
			LPC_GPIO0->FIOCLR |= (1<<0);
			LPC_GPIOINT->IO0IntClr |= (1<<1);
		}
	}
	if(LPC_GPIOINT->IntStatus&&(1<<2)){
		//Me conviene hacer una cadena de IF ELSE anidados y que siempre al final limpie la bandera
		// qué pasa si entran dos interrupciones?  Me jodo
		int columna=0;
		int fila =0;
		int vector_interrupcion = LPC_GPIOINT->IO2IntStatF;
		retardo(364500);
		int vector_interrupcion2 = LPC_GPIOINT->IO2IntStatF;
		retardo(364500*5);
		int vector_interrupcion3 = LPC_GPIOINT->IO2IntStatF;
		int vector_estado = LPC_GPIO2->FIOPIN&0xF;
		retardo(364500);
		int vector_estado1 = LPC_GPIO2->FIOPIN&0xF;
		retardo(364500);
		int vector_estado2 = LPC_GPIO2->FIOPIN&0xF;
		retardo(364500);
		if(LPC_GPIOINT->IO2IntStatF &(1<<0)){
			columna=0;
			if(antideboucing(0)){
				fila = buscarFila(0);
			}
			//LPC_GPIOINT->IO2IntClr |= (1<<0);
		}
		else{
			if(LPC_GPIOINT->IO2IntStatF &(1<<1)){
				columna=1;
				if(antideboucing(1)){
					fila = buscarFila(1);
				}
				//LPC_GPIOINT->IO2IntClr |= (1<<1);
			}
			else{
				if(LPC_GPIOINT->IO2IntStatF &(1<<2)){
					columna=2;
					if(antideboucing(2)){
						fila = buscarFila(2);
					}
					//LPC_GPIOINT->IO2IntClr |= (1<<2);
				}
				else{
					if(LPC_GPIOINT->IO2IntStatF &(1<<3)){
						columna=3;
						if(antideboucing(3)){
							fila = buscarFila(3);
						}
						//LPC_GPIOINT->IO2IntClr |= (1<<3);
					}
				}
			}
		}
		LPC_GPIOINT->IO2IntClr |= (0xF<<0);
		Contador_Entradas++;
		armarInstruccion(TECLADO_entradas[3-fila][3-columna]);
		while(!(LPC_UART0->LSR &(1<<5))){}
			LPC_UART0->THR = TECLADO_entradas[3-fila][3-columna];
		retardo(364500);
	}
}

void TX_enviar(char cadena []){
	while(!(LPC_UART0->LSR &(1<<5))){}
	LPC_UART0->THR = 10;
	for (int var = 0; var < strlen(cadena); ++var) {
		while(!(LPC_UART0->LSR &(1<<5))){}
		LPC_UART0->THR = cadena[var];
	}
	while(!(LPC_UART0->LSR &(1<<5))){}
	LPC_UART0->THR = 10;
	salida_TX[0]='\0';
}

void actualizarCadena(int hour, int mint, int sec,char * destino,int tamano){
	char cadena [tamano];
	cadena[0] = '\0';
	char auxiliar [2];
	auxiliar[0]='\0';
	char hora[3];
	hora[0]='\0';
	if(hour<10){
		strcat(hora,"0");
		itoa(hour,auxiliar,10);
		strcat(hora,auxiliar);
	}
	else{
		itoa(hour,hora,10);
	}
	char minutos[3];
	minutos[0]='\0';
	auxiliar[0]='\0';
	if(mint<10){
		strcat(minutos,"0");
		itoa(mint,auxiliar,10);
		strcat(minutos,auxiliar);
	}
	else{
		itoa(mint,minutos,10);
	}
	char segundos[3];
	segundos[0]='\0';
	auxiliar[0]='\0';
	if(sec<10){
		strcat(segundos,"0");
		itoa(sec,auxiliar,10);
		strcat(segundos,auxiliar);
	}
	else{
		itoa(sec,segundos,10);
	}
	strcat(cadena,hora);
	strcat(cadena,":");
	strcat(cadena,minutos);
	if(tamano==9){
		strcat(cadena,":");
		strcat(cadena,segundos);
	}
	strcpy(destino,cadena);
	return;
}

void BuzzerConfig(void){
	/*
	 * Definicion de P0.1 y P0.0
	 * P0.1 entrada switch, varía entre 1 prendido y 0 apagado
	 * P0.0 como salida al buzzer
	 */
	LPC_PINCON->PINSEL0 &= ~(3<<0);
	LPC_GPIO0->FIODIR |= (1<<0);				//P0.0 como salida
	LPC_PINCON->PINSEL0 &= ~(3<<2);				//Habilito P0.1 como GPIO
	LPC_GPIO0->FIODIR &= ~(1<<1);				//Como entrada
	LPC_PINCON->PINMODE0 &=~(3<<2);
	LPC_GPIOINT->IO0IntEnF = (1<<1);			//Habilito interrupcion por flanco de bajada
}

void ADC_config(void){
	LPC_SC->PCONP |= (1<<12);					//Dejo el PCLKSEL por defecto = cclk/4
	LPC_ADC->ADCR |= (1<<21);					//ADC operacional
	LPC_ADC->ADCR &=~(1<<0);					//Desactivo el modo BURST
	LPC_PINCON->PINSEL1 |= (1<<14);				//Configuro el P0.23 como AD0.0
	LPC_PINCON->PINMODE1|= (1<<15);				//Ni pull up ni push down
	LPC_ADC->ADCR |= (1<<0);					//Eligo sólo el canal 0;
	LPC_ADC->ADINTEN |= (1<<0);					//Habilito interrupcion por fin de conver AD0.0
	LPC_ADC->ADINTEN &= ~(1<<8);
}

void ADC_IRQHandler(){
	static int contador_ADC=0;
	contador_ADC++;
	if(((LPC_ADC->ADDR0>>4)&(0xFFF))<0x7FF){
		LPC_GPIO0->FIOSET |= (1<<0);
	}
	else{
		LPC_RTC->ALHOUR=ALARM_HOUR_BACKUP;
		LPC_RTC->ALMIN =ALARM_MIN_BACKUP;
	}
	LPC_ADC->ADCR &= ~(1<<24);
}

void ADC_inicioConversion(void){
	LPC_ADC->ADCR |= (1<<24);
}

void TIMER_config(){
	//Por default timer 0 enable por reset
	//dejar el pclk = cclk/4
	LPC_TIM0->TCR |= (1<<1);					//reset del timer
	LPC_TIM0->TCR &= ~(1<<1);
	LPC_TIM0->CTCR &= ~(3<<0);
	LPC_TIM0->PR = (SystemCoreClock/4)-1;
	LPC_TIM0->MCR |= (7<<0);
	LPC_TIM0->MR0 = 30;
}

void TIMER0_IRQHandler(void){
	LPC_GPIO0->FIOCLR |= (1<<0);			//apago el buzzer
	LPC_TIM0->IR |= (1<<0);					//Limpiar bandera
}

void Buzzer_sonar(void){
	//no solo prende el buzzer sino inicia su timer
	LPC_GPIO0->FIOSET |=(1<<0);
	LPC_TIM0->TCR |= (1<<0);				//contador habilitado
}

void config_Teclado_Matricial(void){
	//Por default en el PINSEL están como GPIO y con PINMODE pull up
	LPC_GPIO2->FIODIR |= (0xF<<4);			//P2.4 al P2.7 como salida
	LPC_GPIO2->FIOCLR |= (0xF<<4);			//Pongo en bajo las salidas
	LPC_GPIOINT->IO2IntEnF |= (0xF<<0);
	LPC_GPIOINT->IO2IntClr |= (0xF<<0);
}

int buscarFila(int columna){
	for (int var = 0; var < 4; ++var) {
		LPC_GPIO2->FIOSET |= (0xF<<4);				//Primero prendo todas
		LPC_GPIO2->FIOCLR |= (1<<(4+var));			//Comienzo apagando una
		if(!(LPC_GPIO2->FIOPIN &(1<<columna))){		//Sigue en 0?
			LPC_GPIO2->FIOCLR |= (0xF<<4);
			return var;
		}
	}
	return 255;										//No debería nunca llegar acá
}

int antideboucing(int columna){
	/*@param columna	indica la columna (entrada) que pasó por flanco de bajada
	 *@return true es que fue por cambio de estado
	 *@return false porque se debe a ruido
	 * Verifico que la interrupción por flanco de bajada haya sido por un cambio a estado 0
	 * y no por ruido, muestreando luego de 25 milisegundos el estado del pin
	 */
	retardo(364500);
	if(!(LPC_GPIO2->FIOPIN & (1<<columna))){
		return 1;
	}
	else{
		return 0;
	}
}

void retardo(int tiempo){
	for (int var = 0; var < tiempo; ++var) {}
}
